
<?php


   use yii\grid\GridView;
    use yii\widgets\Pjax;
    ?>
    

<div class="jumbotron">
    <h2>
        <?=$titulo?>
    </h2>
    
    <p class="lead">
        <?= $enunciado?>
        
    </p>
    
    <div class="well">
        <?= $sql ?>
    </div>
</div>

 <?= GridView::widget([
    'dataProvider' => $resultados,
    'columns' =>$campos,
    
]);?>
