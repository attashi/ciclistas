<?php

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><b>Consultas ciclistas 1</b></h1>

        <p class="lead">Consultas de la base de datos ciclista</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 1 </h4>
                    <p> listar edades de los ciclistas </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consultax'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- botnon de la primera consulta fin -->
            
             <div class="col-sm-2 col-md-4 col-lg 4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 2 </h4>
                    <p> listar las edades de los ciclistas de Artiach </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta2d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 2 fin -->
            
             <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 3 </h4>
                    <p> listar las edades de los ciclistas de Artiach o de Amore Vita </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta3d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 3 fin -->
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 4 </h4>
                    <p> listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30 </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta4d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 4 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 5 </h4>
                    <p> listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto
 </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta5d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 5 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 6 </h4>
                    <p> Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8 </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta6d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 6 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 7 </h4>
                    <p> lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta7d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 7 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 8 </h4>
                    <p> Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta8d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 8 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 9 </h4>
                    <p> Listar el nombre de los puertos cuya altura sea mayor de 1500  </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta9d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 9 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 10 </h4>
                    <p>  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta10d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!-- boton 10 fin -->
            
            <div class="col-sm-2 col-md-4 col-lg-4">
                <div class="thumbnail">
                <div class="caption">
                    <h4> Consulta 11 </h4>
                    <p> Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000 </p>
                    <p>
                        <?= Html::a('Active record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta11d'], ['class' => 'btn btn-default'])?>
                    </p>
                    
                </div>
                </div>
                </div>
            <!--ultimo boton -->
            </div>
    </div>
            
              